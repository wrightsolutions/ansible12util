#!/bin/bash
mkdir ./host_vars
[ $? -gt 1 ] && exit 101
mkdir ./group_vars
[ $? -gt 1 ] && exit 102

mk_subdirs () {
    SUBDIRS_="files templates handlers tasks vars"
    for subdir in $SUBDIRS_
    do
	mkdir ./roles/$role/$subdir
    done
}

mkdir ./roles
ROLES_="common centos6 debian7 debian8"
for role in $ROLES_
do
    #printf $role
    mkdir ./roles/$role
    mk_subdirs
done
